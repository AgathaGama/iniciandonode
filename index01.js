let express = require('express');
let app = express();

const port = 8000;

//recurso principal
app.get('/', (resquest, response) =>{
response.send("{message: método get principal}");

});

//recurso funcionario 
app.get('/funcionario', (resquest, response) =>{
    let obj = resquest.query;
    let nome = obj.nome;
    let sobrenome = obj.sobrenome;
    response.send("{message: método get funcionario "+ nome + "}");

});

// habilitando o serviço na porta 8000
app.listen(port, function(){
    console.log("Projeto rodando na porta: " + port)
});